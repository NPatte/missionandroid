package com.example.sio2.gestionsejour;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by sio2 on 21/03/17.
 */

public class SortiePatientActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sortie);

        Button btnConnexion = (Button) findViewById(R.id.btn_Valider);
        btnConnexion.setOnClickListener(new View.OnClickListener() {

            public void onClick(View ve) {
                Intent intent = new Intent(SortiePatientActivity.this, MainActivity.class);
                Toast.makeText(SortiePatientActivity.this, "Le patient est sortie", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });

        Button btnAnnuler = (Button) findViewById(R.id.btn_Annuler);
        btnAnnuler.setOnClickListener(new View.OnClickListener() {

            public void onClick(View ve) {
                Intent intent = new Intent(SortiePatientActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
