package com.example.sio2.gestionsejour;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.os.AsyncTask;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button btn_liste_patient = (Button) findViewById(R.id.btn_listePatients);
        btn_liste_patient.setOnClickListener(new View.OnClickListener() {

            public void onClick(View ve) {
                Intent intent = new Intent(MainActivity.this, ListePatientActivity.class);
                startActivity(intent);

            }
        });

        Button btn_Arrivee_Patient = (Button) findViewById(R.id.btn_Arrivee);
        btn_Arrivee_Patient.setOnClickListener(new View.OnClickListener() {

            public void onClick(View ve) {
                Intent intent = new Intent(MainActivity.this, ArriveePatientActivity.class);
                startActivity(intent);
            }
        });

        Button btn_SortiePatient = (Button) findViewById(R.id.btn_Sortie);
        btn_SortiePatient.setOnClickListener(new View.OnClickListener() {

            public void onClick(View ve) {
                Intent intent = new Intent(MainActivity.this, SortiePatientActivity.class);
                startActivity(intent);
            }
        });
    }







}



