package com.example.sio2.gestionsejour;

import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Date;

/**
 * Created by sio2 on 21/03/17.
 */

public class ArriveePatientActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arrivee);

        Button btnConnexion = (Button) findViewById(R.id.btn_Valider);
        btnConnexion.setOnClickListener(new View.OnClickListener() {

            public void onClick(View ve) {
                Intent intent = new Intent(ArriveePatientActivity.this, MainActivity.class);
                Toast.makeText(ArriveePatientActivity.this, "Le patient est bien arrivé", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });

        Button btnAnnuler = (Button) findViewById(R.id.btn_Annuler);
        btnAnnuler.setOnClickListener(new View.OnClickListener() {

            public void onClick(View ve) {
                Intent intent = new Intent(ArriveePatientActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
