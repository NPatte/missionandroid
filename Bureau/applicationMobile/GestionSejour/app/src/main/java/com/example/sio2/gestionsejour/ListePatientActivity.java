package com.example.sio2.gestionsejour;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by sio2 on 21/03/17.
 */

public class ListePatientActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lt_patient);

        ltPatient();
        Button btnAnnuler = (Button) findViewById(R.id.btn_accueil);
        btnAnnuler.setOnClickListener(new View.OnClickListener() {

            public void onClick(View ve) {
                Intent intent = new Intent(ListePatientActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void ltPatient()
    {
        TextView affiche=(TextView) findViewById(R.id.affiche);
        tacheAsync maTache = new tacheAsync();
        maTache.execute();
    }
    public class tacheAsync extends AsyncTask<String, Integer, String>
    {
        @Override
        protected String doInBackground(String... arg0)
        {
            String aRetourner= "";
            URL url;
            StringBuffer leBuffer = new StringBuffer(aRetourner);
            try{
                url = new URL("http://10.0.2.2/missionData/test.php");
                HttpURLConnection conn =(HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                System.out.println("response Code: "+conn.getResponseCode());
                InputStream leFluxEntree = new BufferedInputStream(conn.getInputStream());
                BufferedReader leLecteur=new BufferedReader(new InputStreamReader(leFluxEntree));
                String laLigne = leLecteur.readLine();
                while (laLigne!=null)
                {
                    leBuffer.append(laLigne);
                    leBuffer.append("\n");
                    laLigne = leLecteur.readLine();
                }
                aRetourner = leBuffer.toString();
            }

            catch(Exception e)
            {
                e.printStackTrace();
                aRetourner="erreur";
            }
            return aRetourner;
        }

        protected void onPostExecute(String pResult)
        {
            JSONObject lObjet;
            JSONArray tous;
            String aAfficher="";
            ArrayList<Patient> lesPatients=new ArrayList<Patient>();
            Patient lePatient=null;
            EditText recherchePatient = (EditText) findViewById(R.id.saisieRecherche);
            try{
                tous=new JSONArray(pResult);
                for(int i=0;i<tous.length();i++)
                {
                    lObjet=tous.getJSONObject(i);
                    lePatient=new Patient(lObjet.getInt("id"),lObjet.getString("nom"),lObjet.getString("prenom"),lObjet.getString("adresse"),lObjet.getString("telephone"));
                    aAfficher+="\n"+lePatient.toString();
                    lesPatients.add(lePatient);
                }

            }
            catch (Exception e)
            {
                aAfficher+="\n erreur de récup"+e.getMessage();
            }

            TextView txtVw = (TextView) findViewById(R.id.affiche);
            txtVw.setText(aAfficher);
        }
    }
    public class Patient
    {
        private int id;
        private String nom;
        private String prenom;
        private String adresse;
        private String telephone;


        public Patient(int id, String nom, String prenom,String adresse,String telephone) {
            this.id = id;
            this.nom = nom;
            this.prenom = prenom;
            this.adresse = adresse;
            this.telephone = telephone;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getPrenom() {
            return prenom;
        }

        public void setPrenom(String prenom) {
            this.prenom = prenom;
        }

        public String getAdresse() { return adresse; }

        public void setAdresse(String adresse) {
            this.adresse = adresse;
        }

        public String getTelephone() { return telephone; }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        @Override
        public String toString() {
            return
                    " id=" + id + "\n" +
                            " nom=" + nom + "\n" +
                            " prenom=" + prenom  + "\n" +
                            " adresse=" + adresse  + "\n" +
                            " telephone=" + telephone +
                             "\n" + "\n";
        }
    }

}


