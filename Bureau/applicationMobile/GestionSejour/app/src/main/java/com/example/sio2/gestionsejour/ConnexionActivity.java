package com.example.sio2.gestionsejour;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by sio2 on 21/03/17.
 */

public class ConnexionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        Button btnConnexion = (Button) findViewById(R.id.button_Valider_Connexion);
        btnConnexion.setOnClickListener(new View.OnClickListener() {

            public void onClick(View ve) {
                EditText login = (EditText) findViewById(R.id.saisie_Login);
                String pLogin=login.getText().toString();

                EditText MDP = (EditText) findViewById(R.id.saisie_MDP);
                String pMDP=MDP.getText().toString();

                if (pLogin.equals("admin") && pMDP.equals("admin"))
                {

                            Toast.makeText(ConnexionActivity.this, "Connexion effectuer", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(ConnexionActivity.this, MainActivity.class);
                            startActivity(intent);
                }
                else
                {
                    Toast.makeText(ConnexionActivity.this, "echec de connexion Mot de passe ou login faux", Toast.LENGTH_LONG).show();
                }
            }
        });


    }
}
